package com.company.strategy;

/**
 * 候选人枚举
 *
 */
public enum CandidateEnum {
    ChuanPu("1", "川普"), BaiDeng("2", "拜登");

    private String code;
    private String description;

    CandidateEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
