package com.company.strategy;

import java.util.HashMap;
import java.util.Map;

public class StrategyFactory {
    //单例
    private static StrategyFactory factory = new StrategyFactory();

    private StrategyFactory() {
    }

    private static Map<String, PresidentStrategy> strategyMap = new HashMap<>();

    static {
        strategyMap.put(CandidateEnum.ChuanPu.getCode(), new ChuanPu());
        strategyMap.put(CandidateEnum.BaiDeng.getCode(), new BaiDeng());
    }

    public PresidentStrategy creator(String type) {
        return strategyMap.get(type);
    }

    public static StrategyFactory getInstance() {
        return factory;
    }
}
