package com.company.strategy;

/**
 * 上下文对象
 */
public class Context {
    //依赖抽象
    private PresidentStrategy strategy;

    public void action(String type) {
        //根据候选人去选择具体的策略实现类
        strategy = StrategyFactory.getInstance().creator(type);
        strategy.action();
    }

    public PresidentStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(PresidentStrategy strategy) {
        this.strategy = strategy;
    }
}
