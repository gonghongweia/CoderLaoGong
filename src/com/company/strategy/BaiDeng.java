package com.company.strategy;

/**
 * 拜登实现类
 */
public class BaiDeng implements PresidentStrategy {
    @Override
    public void action() {
        System.out.println("我是拜登，我好战！！");
    }
}
