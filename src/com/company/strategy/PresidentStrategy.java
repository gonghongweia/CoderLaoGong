package com.company.strategy;

/**
 * 总统策略接口
 */
public interface PresidentStrategy {
    void action();
}
