package com.company.strategy;

/**
 * 客户端 调用选举方法
 */
public class Client {
    public static void main(String[] args) {
        Context context = new Context();
        //传入候选人
        context.action(CandidateEnum.BaiDeng.getCode());
    }
}
