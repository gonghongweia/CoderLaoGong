package com.company.staticproxy;

/**
 * 真实对象（被代理对象）：小宫
 *
 */
public class Buyer implements IShopping{

	/**
	 * 核心业务：付钱买口红
	 */
	@Override
	public void Buylipstick() {
		System.out.println("小宫付钱买下了口红");
	}

}
