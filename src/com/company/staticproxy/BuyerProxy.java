package com.company.staticproxy;

/**
 * 代理对象实现类
 */
public class BuyerProxy implements IShopping {

	/**
	 * target 真实对象  被代理人
	 */
	private IShopping target;

	public BuyerProxy(IShopping target) {
		super();
		this.target = target;
	}

	@Override
	public void Buylipstick() {

		System.out.println("了解色号、品牌爆款，评估小宫是否适合买口红");//前置增强

		target.Buylipstick(); //核心业务仍然由被代理对象执行（小宫付钱）

		System.out.println("付钱后代理朋友圈晒订单，满足小宫虚荣心");//后置增强
	}

}
