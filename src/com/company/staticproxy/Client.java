package com.company.staticproxy;


/**
 * 客户端调用方
 */
public class Client {
	public static void main(String[] args) {
		//创建目标对象(被代理对象)
		Buyer gong= new Buyer();

		//创建代理对象, 同时将被代理对象传递给代理对象
		BuyerProxy buyerProxy = new BuyerProxy(gong);

		//通过代理对象，调用到被代理对象的方法
		//即：执行的是代理对象的方法，代理对象再去调用目标对象的方法
		buyerProxy.Buylipstick();
	}
}
